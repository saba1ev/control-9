import  axios from 'axios';

const contact = axios.create({
  baseURL: 'https://control-sobolev.firebaseio.com/'
});

export default contact;