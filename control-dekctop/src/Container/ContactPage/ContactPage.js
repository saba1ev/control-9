import React, {Component} from 'react';
import {NavLink as RouterLink} from "react-router-dom";
import {Container, NavLink} from 'reactstrap'
import './ContactPage.css';
import {connect} from "react-redux";
import {close_contact, fetch_contact, fetch_delete, open_contact} from "../../store/actions";
import Modal from '../../components/UI/Modal/Modal';
import axios from '../../axios-contacts';

class ContactPage extends Component {
  state={
    oneContact: []
  }
  componentDidMount(){
    this.props.getContact();
  }
  getconsole = (number) =>{
     this.setState({oneContact: number})
  }
  render() {
    let contactShow = [];
    let keyDelete = '';
    for (let key in this.props.contact) {
      keyDelete = key;
      contactShow.push(<div key={key} onClick={()=>this.getconsole(this.props.contact[key])} >
        <div onClick={()=>this.props.open()} className='ContactBox'>
          <img src={this.props.contact[key].uri} alt=""/>
          <h3>{this.props.contact[key].name}</h3>
        </div>


      </div>);
    }


    return (
      <div>
        <div className='Header-Contact'>
          <Container>
            <div className="NavBar">
              <h2>Contact</h2>
              <NavLink tag={RouterLink} to='/addContact'>Add New Contact</NavLink>
            </div>
            <div className='ContactDask'>
              {contactShow}
            </div>
            <Modal show={this.props.modal === true} close={()=>this.props.close()}>
              <img src={this.state.oneContact.uri} alt="" className='ImgModal'/>
              <p>{this.state.oneContact.name}</p>
              <p>{this.state.oneContact.email}</p>
              <p>{this.state.oneContact.telephone}</p>
              <button onClick={()=>this.props.deleteContact(keyDelete)}>delete</button>
            </Modal>
          </Container>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) =>{
  return{
    contact: state.contact.contact,
    modal: state.contact.modal
  }
};

const mapDispatchToProps = (dispatch) =>{
  return{
    getContact: ()=> dispatch(fetch_contact()),
    open: ()=> dispatch(open_contact()),
    close: () => dispatch(close_contact()),
    deleteContact: (id)=> dispatch(fetch_delete(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps) (ContactPage);