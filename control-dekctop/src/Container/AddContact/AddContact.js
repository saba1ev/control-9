import React, {Component} from 'react';
import './AddContact.css'
import {NavLink as RouterLink} from "react-router-dom";
import {NavLink} from 'reactstrap';
import axios from '../../axios-contacts';

class AddContact extends Component {
  state = {
    name: '',
    telephone: '',
    email: '',
    uri: '',
  };

  changeHendler = (event) =>{
    const {name, value} = event.target;
    this.setState({[name]: value})
  }

  pushContact = () =>{
    const contact = {...this.state};
    axios.post(`/contact.json`, contact).then(() =>{
      this.props.history.push('/')
    })
  };

  render() {
    return (
      <div>
        <h2>Add new Contact</h2>
        <form className='form-Contact'>
          <input type="text" name='name' placeholder='Press contact name' value={this.state.name} onChange={this.changeHendler}/>
          <input type="tel" name='telephone' placeholder='Press telephone number' value={this.state.telephone} onChange={this.changeHendler}/>
          <input type="email" name='email' placeholder='Press Email' value={this.state.email} onChange={this.changeHendler}/>
          <input type="url" name='uri' placeholder='Put url photo' value={this.state.uri} onChange={this.changeHendler}/>
        </form>
        <div className='Prev'>
          <p>Photo Preview:</p>
          <img src={this.state.uri} alt=""/>
        </div>
        <div className='btn'>
          <button onClick={()=>this.pushContact()}>save</button>
          <NavLink tag={RouterLink} to='/'><button>go back</button></NavLink>
        </div>

      </div>
    );
  }
}

export default AddContact;