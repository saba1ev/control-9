import React, {Component, Fragment} from 'react';
import {Switch, Route} from 'react-router-dom';
import './App.css';
import ContactPage from "./Container/ContactPage/ContactPage";
import AddContact from "./Container/AddContact/AddContact";

class App extends Component {
  render() {
    return (
     <Fragment>
        <Switch>
          <Route path='/addContact' component={AddContact}/>
          <Route path='/' exact component={ContactPage}/>
        </Switch>
     </Fragment>
    );
  }
}

export default App;
