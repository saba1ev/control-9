import axios from '../axios-contacts'

export const FETCH_CONTACT_REQUEST = 'FETCH_CONTACT_REQUEST';
export const FETCH_CONTACT_SUCCESS = 'FETCH_CONTACT_SUCCESS';
export const FETCH_CONTACT_FAILURE = 'FETCH_CONTACT_FAILURE';


export const fetch_contact_request = () =>{
  return {type: FETCH_CONTACT_REQUEST}
};
export const fetch_contact_success = (contact) =>{
  return {type: FETCH_CONTACT_SUCCESS, contact}
};
export const fetch_contact_failure = (failure) =>{
  return {type: FETCH_CONTACT_FAILURE, failure}
};


export const OPEN_CONTACT = 'OPEN_CONTACT';
export const CLOSE_CONTACT = 'CLOSE_CONTACT';


export const open_contact = () =>{
  return{type: OPEN_CONTACT}
};
export const close_contact = () =>{
  return{type: CLOSE_CONTACT}
};


export const fetch_delete = (id) =>{;
  return dispatch =>{
    dispatch(fetch_contact_request());
    return axios.delete(`/contact/${id}.json`).then(()=>{
      dispatch(fetch_contact())
    })
  }
}


export const fetch_contact = () =>{
  return dispatch => {
    dispatch(fetch_contact_request());
    return axios.get('/contact.json').then(res=>{
      dispatch(fetch_contact_success(res.data))
    },error=>{
      dispatch(fetch_contact_failure(error))
    })
  }
};

export const fetch_get_contact = (id) => {
  return dispatch => {
    dispatch(fetch_contact_request());
    return axios.get(`/contact/${id}.json`).then(res => {
      dispatch(fetch_contact_success(res.data))
    })
  }
};
