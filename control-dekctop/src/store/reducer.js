import {
  CLOSE_CONTACT,
  FETCH_CONTACT_FAILURE,
  FETCH_CONTACT_REQUEST,
  FETCH_CONTACT_SUCCESS,
  OPEN_CONTACT
} from "./actions";

const initialState ={
  contact: null,
  failure: null,
  loading: false,
  modal: false
};

export const reducer = (state = initialState, action) =>{
  switch (action.type) {
    case FETCH_CONTACT_REQUEST:
      return{
        ...state,
        loading: true,
      };
    case FETCH_CONTACT_SUCCESS:
      return{
        ...state,
        contact: action.contact,
        loading: false,
      };
    case FETCH_CONTACT_FAILURE:
      return{
        ...state,
        failure: action.failure,
        loading: false
      };
    case OPEN_CONTACT:
      return{
        ...state,
        info: action.key,
        modal: true,

      };
    case CLOSE_CONTACT:
      return{
        ...state,
        modal: false
      };
    default:
      return state

  }
};
