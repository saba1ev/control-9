import axios from 'axios';

const basic = axios.create({
  baseURL: 'https://control-sobolev.firebaseio.com/'
});

export default basic;