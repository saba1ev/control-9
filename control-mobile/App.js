import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import axios from './axios-contact'
export default class App extends React.Component {
  state = {
    contact: [],
  };


  getContact = () =>{
    axios.get('/contact.json').then(response=>{
      return (this.setState({contact: response.data}));
    })
  };
  componentDidMount(){
    this.getContact()
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.contact.map(item=>{
          return(
            <View key={item}>
              <Text>{item.name}</Text>
            </View>
          )
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
